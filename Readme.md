# The .attheme glossary database

This repository contains the database of .attheme glossary. The database is the
core part of the glossary, which is shared between the [website] and
@atthemeglossarybot, and you can build your tools on top of the database, too.
See the [Readme][database readme] for the database for more.

In addition, the repository also provides a parser of the database implemented
in Rust. See the [Readme][parser readme] for the parser for more.

[website]: http://attheme-glossary.snejugal.ru
[database readme]: ./database/Readme.md
[parser readme]: ./parser/Readme.md
