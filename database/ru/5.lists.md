# Списки

<glossary-variable color="red">

## divider

Задает цвет черты разделяющей элементы списка.

</glossary-variable>

<glossary-variable color="blue">

## listSelectorSDK21

Задает цвет эффекта нажатия на элемент списка.

</glossary-variable>

<glossary-variable color="green">

## emptyListPlaceholder

Задает цвет текста подсказки, которая появляется, когда вы не добавили еще ни
одного файла в список. Например, на экране Чата → Прикрепить файлы → Музыка,
если у вас нет музыки.

</glossary-variable>

<figure>

![](./images/list.0.png)

<figcaption>

Красное — `divider`, синее — `listSelectorSDK21`, зеленое —
`emptyListPlaceholder`.

</figcaption>
</figure>
