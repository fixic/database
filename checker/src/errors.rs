use std::{io::Write, ops::RangeBounds, path::Path};
use termcolor::{Color, ColorSpec, StandardStream, WriteColor};

pub mod advanced_check_error;
pub mod parse_error;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct Highlight<R: RangeBounds<usize>> {
    range: R,
    color: Color,
}

fn print_path(stderr: &mut StandardStream, path: &Path) {
    let mut gray = ColorSpec::new();
    gray.set_fg(Some(Color::Black));
    gray.set_intense(true);

    stderr.set_color(&gray).unwrap();
    write!(stderr, "   -->").unwrap();

    stderr.reset().unwrap();
    writeln!(stderr, " {}", path.to_string_lossy()).unwrap();
}

fn print_error(stderr: &mut StandardStream, error: &str) {
    print_badge(stderr, "error", Color::Red);
    writeln!(stderr, "{}", error).unwrap();
}

fn print_help(stderr: &mut StandardStream, help: &str) {
    print_badge(stderr, "help", Color::Green);
    writeln!(stderr, "{}", help).unwrap();
    stderr.reset().unwrap();
}

fn print_badge(stderr: &mut StandardStream, badge: &'static str, color: Color) {
    stderr
        .set_color(ColorSpec::new().set_fg(Some(color)).set_bold(true))
        .unwrap();
    write!(stderr, "{:>5}", badge).unwrap();

    stderr.set_color(ColorSpec::new().set_bold(true)).unwrap();
    write!(stderr, ": ").unwrap();
}

fn print_file<'a, I, R>(
    stderr: &mut StandardStream,
    mut file: I,
    first_line_index: usize,
    highlight: Option<Highlight<R>>,
) where
    I: Iterator<Item = &'a str>,
    R: RangeBounds<usize>,
{
    let choose_color = |stderr: &mut StandardStream, index| {
        let mut spec = ColorSpec::new();
        spec.set_bold(true);
        match &highlight {
            Some(Highlight { range, color }) if range.contains(&index) => {
                spec.set_fg(Some(*color));
            }
            _ => {
                spec.set_fg(Some(Color::Black));
                spec.set_intense(true);
            }
        }

        stderr.set_color(&spec).unwrap();
    };

    if first_line_index > 0 {
        choose_color(stderr, first_line_index - 1);
        writeln!(stderr, " ... │ ").unwrap();
    }

    for (offset, line) in (&mut file).enumerate().take(5) {
        let line_index = offset + first_line_index;

        choose_color(stderr, line_index);
        write!(stderr, "{:>4} │ ", line_index + 1).unwrap();

        stderr.reset().unwrap();
        writeln!(stderr, "{}", line).unwrap();
    }

    if file.next().is_some() {
        choose_color(stderr, first_line_index + 6);
        writeln!(stderr, " ... │ ").unwrap();
        stderr.reset().unwrap();
    }
}
