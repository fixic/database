use super::{print_error, print_file, print_help, print_path, Highlight};
use attheme_glossary_parser::parse_error::{Kind, ParseError};
use inflector::Inflector;
use std::{io::Write, iter::once, ops::RangeFull};
use termcolor::{Color, ColorChoice, StandardStream};

const INVALID_VARIABLE_TAG_HELP: &str =
    "The opening tag must look like `<glossary-variable color=\"{color}\">`, \
     where `{color}` is one of these: `red`, `pink`, `purple`, `deepPurple`, \
     `indigo`, `blue`, `lightBlue`, `cyan`, `teal`, `green`, `lightGreen`, \
     `lime`, `yellow`, `amber`, `orange`, `deepOrange`, `brown`, `gray`, \
     `blueGray`";

const ALLOWED_HTML_MARKUP: [&str; 2] = [
    "<glossary-variable color=\"{{color}}\">...</glossary-variable>",
    "<figure> ![]() <figcaption>...</figcaption></figure>",
];

pub fn display(error: &ParseError) {
    let stderr = &mut StandardStream::stderr(ColorChoice::Always);

    if error.kind == Kind::NoInfoToml {
        print_error(stderr, "Could not find `info.toml`");
        print_path(stderr, &error.path);
        print_help(
            stderr,
            "Create a file `info.toml` with the following contents:",
        );
        print_file::<_, RangeFull>(
            stderr,
            "language_name = \"...\"\n\
                title = \"...\"\n\
                search = \"...\"
                about = \"...\""
                .lines(),
            0,
            None,
        );
        return;
    }

    if error.kind == Kind::EmptyFile {
        print_error(stderr, "Empty section file");
        print_path(stderr, &error.path);
        print_help(stderr, "Start the section with a heading and contents:");
        print_file::<_, RangeFull>(
            stderr,
            "# Glossary of Android Telegram Themes variables\n\n\

            While creating themes for Android Telegram..."
                .lines(),
            0,
            None,
        );
        return;
    }

    let file = std::fs::read_to_string(&error.path).unwrap();
    let display_original = |stderr| {
        let start = error.line.saturating_sub(2);

        print_file(
            stderr,
            file.lines().skip(start),
            start,
            Some(Highlight {
                range: error.line..=error.line,
                color: Color::Red,
            }),
        );
    };

    match &error.kind {
        Kind::InfoTomlParseError { inner } => {
            let message =
                format!("Failed to parse `info.toml`: {}", inner.to_string());
            print_error(stderr, &message);
            print_path(stderr, &error.path);

            let line_col = inner.line_col();
            let (lines, start, error) = if let Some((line, ..)) = line_col {
                let start = line.saturating_sub(2);
                let lines: Vec<_> = file.lines().skip(start).take(6).collect();
                let highlight = Highlight {
                    range: line..=line,
                    color: Color::Red,
                };
                (lines, start, Some(highlight))
            } else {
                (file.lines().take(6).collect(), 0, None)
            };

            print_file(stderr, lines.into_iter(), start, error);
        }
        Kind::FileStartsWithNotHeading => {
            print_error(stderr, "Section file doesn't start with a heading");
            print_path(stderr, &error.path);
            display_original(stderr);
            print_help(stderr, "Add a first-level heading:");
            let stem = error.path.file_stem().unwrap().to_string_lossy();
            let name = stem.split('.').nth(1).unwrap();
            let heading = format!("# {}", name.to_sentence_case());

            print_file(
                stderr,
                [heading.as_str(), ""].iter().copied().chain(file.lines()),
                0,
                Some(Highlight {
                    range: 0..=0,
                    color: Color::Green,
                }),
            )
        }
        Kind::InvalidVariableOpeningTag => {
            print_error(stderr, "Invalid <glossary-variable> opening tag");
            print_path(stderr, &error.path);
            display_original(stderr);
            print_help(stderr, INVALID_VARIABLE_TAG_HELP);
        }
        Kind::InvalidVariableColor => {
            print_error(stderr, "Unknown variable color");
            print_path(stderr, &error.path);
            display_original(stderr);
            print_help(stderr, INVALID_VARIABLE_TAG_HELP);
        }
        &Kind::NoVariableName { expected_level } => {
            print_error(
                stderr,
                "Expected a variable name in <glossary-variable>",
            );
            let start = error.line.saturating_sub(2);
            print_path(stderr, &error.path);
            display_original(stderr);

            print_help(stderr, "Add a heading with the variable name:");
            let tag_line = file.lines().nth(error.line).unwrap();
            let heading =
                format!("{} {{variableName}}", "#".repeat(expected_level));
            print_file(
                stderr,
                [tag_line, "", &heading]
                    .iter()
                    .copied()
                    .chain(file.lines().skip(error.line + 1)),
                start + 2,
                Some(Highlight {
                    range: start + 4..=start + 4,
                    color: Color::Green,
                }),
            )
        }
        &Kind::WrongHeadingLevel {
            current_level,
            expected_level,
        } => {
            print_error(stderr, "Wrong heading level");
            let start = error.line.saturating_sub(2);
            print_path(stderr, &error.path);
            display_original(stderr);

            let help = format!(
                "Change {} to {}:",
                "#".repeat(current_level),
                "#".repeat(expected_level)
            );
            print_help(stderr, &help);
            let heading =
                &file.lines().nth(error.line).unwrap()[current_level..];
            let correct_line =
                format!("{}{}", "#".repeat(expected_level), heading);

            print_file(
                stderr,
                file.lines()
                    .skip(start)
                    .take(2)
                    .chain(once(correct_line.as_str()))
                    .chain(file.lines().skip(error.line + 1)),
                start,
                Some(Highlight {
                    range: error.line..=error.line,
                    color: Color::Green,
                }),
            )
        }
        Kind::NoFigureImage => {
            print_error(stderr, "Expected an image in <figure>");
            let start = error.line.saturating_sub(2);
            print_path(stderr, &error.path);
            display_original(stderr);

            print_help(stderr, "Add an image:");
            let tag_line = file.lines().nth(error.line).unwrap();
            print_file(
                stderr,
                [tag_line, "", "![](./images/{image_name}.png)"]
                    .iter()
                    .copied()
                    .chain(file.lines().skip(error.line + 1)),
                start + 2,
                Some(Highlight {
                    range: start + 4..=start + 4,
                    color: Color::Green,
                }),
            )
        }
        Kind::NoFigcaption => {
            print_error(stderr, "Expected a <figcaption> in <figure>");
            print_path(stderr, &error.path);
            display_original(stderr);

            print_help(stderr, "Add a caption:");
            print_file(
                stderr,
                "<figcaption>\n\n\
                 {caption}\n\n\
                 </figcaption>\n\n"
                    .lines(),
                error.line,
                Some(Highlight {
                    range: error.line..=error.line + 5,
                    color: Color::Green,
                }),
            )
        }
        Kind::NoCaption => {
            print_error(stderr, "Expected a caption in <figcaption>");
            print_path(stderr, &error.path);
            display_original(stderr);

            print_help(stderr, "Add a caption:");
            print_file(
                stderr,
                "<figcaption>\n\n\
                 {caption}\n\n\
                 </figcaption>\n\n"
                    .lines(),
                error.line,
                Some(Highlight {
                    range: error.line + 2..=error.line + 2,
                    color: Color::Green,
                }),
            )
        }
        Kind::InvalidCaption => {
            print_error(stderr, "Invalid caption in <figcaption>");
            print_path(stderr, &error.path);
            display_original(stderr);
            print_help(stderr, "A caption can only be a paragraph");
        }
        Kind::InvalidFigureEnd => {
            print_error(stderr, "Invalid end of a figure");
            print_path(stderr, &error.path);
            display_original(stderr);
            print_help(
                stderr,
                "A figure can only end with </figcaption>\\n</figure>",
            );
        }
        Kind::UnknownHtml => {
            print_error(stderr, "Unknown HTML");
            print_path(stderr, &error.path);
            display_original(stderr);

            print_help(stderr, "Only these HTML markups are allowed:");
            ALLOWED_HTML_MARKUP.iter().for_each(|markup| {
                writeln!(stderr, "       {}", markup).unwrap();
            });
        }
        Kind::InlineImage => {
            print_error(stderr, "Inline images are not allowed");
            print_path(stderr, &error.path);
            display_original(stderr);
            print_help(
                stderr,
                "Inline images are forbidden because they can't be rendered \
                 in Telegram in any sane way. Consider describing what's \
                 pictured in the image",
            );
        }
        _ => (),
    }
}
